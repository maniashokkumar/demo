import { inject, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

import { CustomvalidationService } from './customvalidation.service';

describe('CustomvalidationService', () => {
  let service: CustomvalidationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule
      ],
    });
    service = TestBed.inject(CustomvalidationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
  service.patternValidator();
  expect(service.patternValidator()(new FormControl('Passwo@'))).toEqual(
   { 'invalidPassword': true }
  );
});

it('should be created', () => {
  expect(service.patternValidator()(new FormControl('Password24@'))).toEqual(null);
})


});
