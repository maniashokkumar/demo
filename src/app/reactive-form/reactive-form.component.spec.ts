import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomvalidationService } from '../services/customvalidation.service';
import { ReactiveFormComponent } from './reactive-form.component';

describe('ReactiveFormComponent', () => {
  let component: ReactiveFormComponent;
  let fixture: ComponentFixture<ReactiveFormComponent>;
  let router:Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA, CustomvalidationService],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [ ReactiveFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveFormComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create 5 of that', () => {
    const fields = Object.keys(component.registerForm.controls);
    expect(fields.length).toBe(5);
  });

  it('should create "name", "email", "username", "password", "confirmPassword" ', () => {
    const fields = Object.keys(component.registerForm.controls);
    expect(fields).toEqual(['name', 'email', 'username', 'password', 'confirmPassword' ]);
  });

  it('onsubmit function', () => {
    component.onSubmit();
    expect(component.submitted).toEqual(true);
  });

  it('should show news intially ', () => {
    const navigateSpy = jest.spyOn(router,'navigate');
    component.form();
    expect(navigateSpy).toHaveBeenCalledWith(['/success-page']);
  });

  describe('Form validator', () => {
    beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
      fixture = TestBed.createComponent(ReactiveFormComponent);
      component = fixture.componentInstance;
      component.registerForm = fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required]],
      password: ['', Validators.compose([Validators.required]) ],
      confirmPassword: ['', [Validators.required]],
      });
      fixture.detectChanges();
    }));

    it('form invalid when empty', () => {
      expect(component.registerForm.invalid).toBeTruthy();
    });

    it('form invalid when empty', () => {
      component.registerForm.setValue({
        name: "Adhi",
        email: "awe@sd.com",
        username: "Adhi",
        password: "Password24@",
        confirmPassword: "Password24@",
      }) ;
      expect(component.registerForm.valid).toBeTruthy();
    });

  it('onsubmit function check', () => {
      component.registerForm.setValue({
        name: "Adhi",
        email: "awe@sd.com",
        username: "Adhi",
        password: "Password24@",
        confirmPassword: "Password24@",
      }) ;

      component.form = jest.fn();
      const spy = jest.spyOn(component, 'form');
      component.onSubmit();
      expect(spy).toHaveBeenCalledTimes(1);
    });

});

})
