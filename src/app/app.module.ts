﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';;
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component'
;
import { SuccesspageComponent } from './successpage/successpage.component';
import { AboutFormComponent } from './about-form/about-form.component'
@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        NavBarComponent
,
        ReactiveFormComponent ,
        SuccesspageComponent ,
        AboutFormComponent  ],
    providers: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule { };