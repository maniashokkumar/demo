import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutFormComponent } from './about-form/about-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { SuccesspageComponent } from './successpage/successpage.component';


const routes: Routes = [
    { path: '', component: ReactiveFormComponent },
    { path: 'reactive-form', component: AboutFormComponent },
    { path: 'success-page', component: SuccesspageComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }